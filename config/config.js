/**
 * This file/module contains all configuration for the Grunt build.
 */


// Data exports
module.exports = {
 
	// Image compression parameters ------------------------------------
	// See https://github.com/gruntjs/grunt-contrib-imagemin/blob/master/docs/imagemin-options.md
	// For details
	imageOptions: {
		options: {
			// PNG options
			//      from 0 (none) to 7 (best)
			optimizationLevel: 5,
			//      convert png to 8 bits palettes to reduce file size
			pngquant:true,
			// JPG options
			progressive:true,
			//GIF options
			interlaced:true
			
		}
	},
	
	
	// Input/output file paths tasks -----------------------------------
	
	// Default task
	all: {
		expand: true,
		cwd: "src/",
		src: ["**/*.{png,jpg,gif}"],
		dest: "dest/"
	},
	
	SD: {
		expand: true,
		cwd: "src/",
		src: ["**/SD/**/*.{png,jpg,gif}"],
		dest: "dest/"
	},
	HD: {
		expand: true,
		cwd: "src/",
		src: ["**/HD/**/*.{png,jpg,gif}"],
		dest: "dest/"
	}
		
};


// Gloabl setup
var colors = require('colors');
colors.setTheme({
		silly: 'rainbow',
		input: 'grey',
		verbose: 'cyan',
		prompt: 'grey',
		info: 'green',
		data: 'grey',
		help: 'cyan',
		warn: 'yellow',
		debug: 'blue',
		error: 'red'
});

