'use strict';
module.exports = function (grunt) {
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	
	/**
	 * Load in our build configuration file.
	 */
	var userDefinedConfig = require('./config/config.js');
 
	grunt.initConfig({
		// Load Config
    config: userDefinedConfig,
	
		
		imagemin : {
			options: userDefinedConfig.imageOptions.options,
			
			all: {
				files: ['<%= config.all %>']
			},
			SD: {
				files: ['<%= config.SD %>']
			},
			HD: {
				// options can be overridden for specific tasks
				options: {optimizationLevel:7, pngquant:false},
				files: ['<%= config.HD %>']
			}
			
		}
	});
	
	
	// Default tasks
	grunt.registerTask('default', ['help']);
	grunt.registerTask('opti', ['imagemin:all']);
	
	grunt.registerTask('help', 'Usage information.', function() {
		var crlf = "\n";
		grunt.log.writeln("Image optimization tools".help +crlf+
											"  ► Run " + "'grunt opti'".green + 
											" to batch optimize all the image in the original folders and output sized-down version in the optim folder." +crlf+
											"  \u25BA You can also call " + "'grunt imagemin:HD'".help + 
											" to batch optimize only the HD subfolder." +crlf+
											"  \u25BA To setup the file paths, files patterns, or image compression arguments, you can edit the " + "'config/config.js'".help + " file accordingly to your needs.");
    /*
    grunt.log.writeln("pngquant:"  + userDefinedConfig.imageOptions.options.pngquant);
    grunt.log.writeln("optimizationLevel:"  + userDefinedConfig.imageOptions.options.optimizationLevel);
    */
	});
	
	
	
};