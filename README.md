# Image optimization

This micro-setup helps you in automatically optimizing all your PNG, JPG and GIF files. 
Please read the Optimizing and Customizing sections to know where to put your ources and how to change these settings. 


## Required

You should already have [NodeJS](http://nodejs.org/) and [Git](http://git-scm.com/) installed, and the [Grunt command line interface](https://npmjs.org/package/grunt-cli) installed in node.

To make sure you have grunt-cli installed, just type
```
$ npm install -g grunt-cli
```


## Installation 

a.) Clone the repo (shallow copy, to only have the latest state)
```
$ git clone --depth 1 https://bitbucket.org/sportebois/tool-image-optimization.git
```

b.) Ask NPM to get all the dependencies
```
$ npm install
```

c.) Customize

Run `$ npm init` to easily change your specific settings.


## Optimizing your image files

a.) Copy your unoptimized files in a `src/`folder

b.) run `$ grunt opti` to optimize them

c.) grab your optimized files in the `dest/` folder


## Custom configuration

To change settings (folders, image setings, whatever), the first place to look is the `config/config.js` file for the common settings.

If you want to create several tasks to specifically optimize this or that, feel free to play in the `Gruntfile.js` tasks descriptions.
